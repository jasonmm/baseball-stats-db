#!/bin/bash
#
# Usage: ./install.sh
#

# We need the root password throughout so we prompt the user for it if an
# environment variable wasn't found.
if [ -z "$ROOT_PASSWORD" ]; then
    read -s -p "Enter MySQL root user password: " rootpwd
    echo
else
    rootpwd=$ROOT_PASSWORD
fi

# Set the name of the database. Use the environment variable if it is set.
if [ -z "$DATABASE_NAME" ]; then
    databasename="lahman"
else
    databasename=$DATABASE_NAME
fi

# Make sure the source data is up-to-date.
if [ ! -d "baseballdatabank" ]; then
	echo "Cloning the 'baseballdatabank' repository..."
	git clone https://github.com/chadwickbureau/baseballdatabank.git
	cd baseballdatabank
else
	echo "Updating the 'baseballdatabank' repository..."
	cd baseballdatabank
	git pull origin master
fi

# Create the database and tables.
cd core
echo "Creating the database..."
mysql -u root -p$rootpwd -e "DROP DATABASE IF EXISTS $databasename"
mysql -u root -p$rootpwd -e "CREATE DATABASE $databasename"
mysql -u root -p$rootpwd $databasename < ../../create-tables.sql || exit $?

# Import the data into the database.
# Only CSV files contain data so non-CSV files are ignored.
# The table name is the lowercased filename (sans extension).
echo "Importing the data..."
for filename in *; do
	if [ $(echo "$filename" | cut -d. -f2) = "csv" ]; then
		tablename=$(echo "$filename" | tr '[:upper:]' '[:lower:]' | cut -d. -f1)
	else
		continue
	fi
	mysql -u root -p$rootpwd $databasename \
		-e "LOAD DATA INFILE '`pwd`/$filename' REPLACE INTO TABLE $tablename FIELDS TERMINATED BY ',' IGNORE 1 LINES" || exit $?
done
cd ../..

mysql -u root -p$rootpwd $databasename < fangraphs-guts.sql

echo "Finished."
