**This project is archived. Instead see [this simpler method](https://github.com/lawlesst/baseballdb-datasette) using [Datasette](https://datasette.io/).**

# Description

[Homepage](https://jasonmm.gitlab.io/baseball-stats-db/)

Creates a MySQL database of baseball statistics. The database is based
on the [BaseballDatabank data](https://github.com/chadwickbureau/baseballdatabank) with a few additional views.

# Requirements
* [MySQL](https://www.mysql.com/) - the `install.sh` script uses `LOAD DATA INFILE` to import BaseballDatabank's CSV files.

# Usage

```sh
$ git clone https://gitlab.com/jasonmm/baseball-stats-db.git
$ cd baseball-stats-db
$ chmod u+x install.sh
$ # -optional- set environment variables (see below)
$ ./install.sh
```

## Environment Variables

The install script will look for the following environment variables.

* **ROOT_PASSWORD** - the install script will ask for the MySQL *root* user's password if this environment variable does not exist.
* **DATABASE_NAME** - the install script will use 'lahman' as the default database name if this environment variable is not present.
