+++
title = "Baseball Stats Database"
date = "2017-02-16T19:55:17-06:00"

+++

The _Baseball Stats Database_ is a MySQL database based on the [BaseballDatabank data](https://github.com/chadwickbureau/baseballdatabank) with some extra tables and views for more convenient analysis.


