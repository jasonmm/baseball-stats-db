+++
date = "2017-02-16T20:09:34-06:00"
title = "Installation"

+++

## Requirements

* MySQL

Currently MySQL is the only database that is supported.  The `install.sh` script calls the MySQL command-line client program.  The MySQL-specific `LOAD DATA INFILE` command is used to import the CSV files from BaseballDatabank as well.

In addition, you must know the password for the `root` user.  The `install.sh` script uses the `root` user to create the database, tables, and import the data.

## Installation

Run the following commands.  The `install.sh` will ask for the password of the MySQL `root` user in order to create the database.

```sh
$ git clone https://gitlab.com/jasonmm/baseball-stats-db.git
$ cd baseball-stats-db
$ chmod u+x install.sh
$ # -optional- set environment variables
$ ./install.sh
```

### Environment Variables

The install script will look for the following environment variables.

* **ROOT_PASSWORD** - the install script will ask for the MySQL *root* user's password if this environment variable does not exist.
* **DATABASE_NAME** - the install script will use 'lahman' as the default database name if this environment variable is not present.
