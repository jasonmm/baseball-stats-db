+++
date = "2017-02-16T20:23:29-06:00"
title = "Database Contents"

+++


## BaseballDatabank Tables

The majority of the tables are based on the [BaseballDatabank data](https://github.com/chadwickbureau/baseballdatabank).  The table names are lowercased versions of CSV filenames found in the BaseballDatabank repository.

* allstarfull - contains information about players' election to an All-Star game.  It does not contain All-Star batting or pitching statistics.
* appearances - contains game appearance information for each player/year/team.
* awardsmanagers - lists awards given to managers.
* awardsplayers - lists awards given to players.
* awardssharemanagers - contains voting details for manager awards.
* awardsshareplayers - contains voting details for player awards.
* batting - yearly batting statistics for every hitter.
* battingpost - yearly postseason batting statistics for every hitter.
* collegeplaying - lists the years and school each player played for a college team.
* fielding - yearly fielding statistics for every fielder, outfield positions are grouped together.
* fieldingof - yearly counts of the number of games played at each outfield position for players who played at least one outfield position for that year.
* fieldingofsplit - yearly fielding statistics for outfielders separated by the outfield positions.
* hallofffame - Hall Of Fame voting.
* homegames - yearly attendence information for parks.
* managers - yearly information about team managers.
* managershalf - the same information as `managers`, but split into half seasons.
* master - demographic data about every player and manager found in other tables.
* parks - lists of parks where a baseball game has been played.
* pitching - yearly pitching statistics for every pitcher.
* pitchingpost - yearly postseason pitching statistics for every pitcher.
* salaries - player salaries.
* schools - information about schools, useful for joining with the `collegeplaying` table.
* seriespost - postseason series information.
* teams - yearly team information.
* teamsfranchises - franchises, useful for grouping records from the `teams` table.
* teamshalf - yearly team win/loss information split into half seasons.

## Additional Tables

This list is additional tables in the _Baseball Stats Database_ that do not come from BaseballDatabank data.

* guts - contains pre-calculated constants [provided by FanGraphs](http://www.fangraphs.com/guts.aspx?type=cn).

## Views

For convenience, several views are provided in the _Baseball Stats Database_.

* battingcareer - career batting statistics for every hitter.
* battingleague - yearly batting statistics for each league.
* battingmlbperyear - yearly batting statistics for all of MLB.
* battingperpamlbperyear - yearly per plate appearance batting statistics for all of MLB.
* franchises - combined statistics for each franchise, includes win/loss records, postseason series wins, batting and pitching statistics. 
* pitchingcareer - career pitching statistics for every pitcher.


